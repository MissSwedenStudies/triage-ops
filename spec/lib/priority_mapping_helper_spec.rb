# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/priority_mapping_helper'

RSpec.describe PriorityMappingHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include PriorityMappingHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  subject { resource_klass.new(labels) }

  describe '#set_priority_for_bug' do
    context 'severity label exists' do
      let(:labels) { [label_klass.new('severity::1')] }

      context 'priority label applied' do
        context 'correct priority label already' do
          before do
            labels << label_klass.new('priority::1')
          end

          it 'will not set priority' do
            expect(subject.set_priority_for_bug).to eq('')
          end
        end

        context 'mismatched priority label' do
          before do
            labels << label_klass.new('priority::2')
          end

          it 'will not override priority' do
            expect(subject.set_priority_for_bug).to eq('')
          end
        end
      end

      context 'priority label not applied' do
        it 'will set correct priority' do
          expect(subject.set_priority_for_bug).to eq('/label ~"priority::1"')
        end
      end
    end

    context 'severity label does not exist' do
      it 'will not set priority' do
        expect(subject.set_priority_for_bug).to eq('')
      end
    end

    context 'out of scope severity label exists' do
      let(:labels) { [label_klass.new('severity::4')] }

      it 'will not set priority' do
        expect(subject.set_priority_for_bug).to eq('')
      end
    end
  end
end